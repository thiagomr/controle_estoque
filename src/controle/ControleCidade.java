/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.ModeloCidade;

/**
 *
 * @author thiag
 */
public class ControleCidade {
    ConectaBanco connCidade = new ConectaBanco();
    public void InserirCidade(ModeloCidade mod){
        connCidade.conexao();
        try {
            //JOptionPane.showMessageDialog(null,mod.getNome());

            PreparedStatement pst = connCidade.conn.prepareStatement("insert into cidade(nome_cidades,id_estado) values(?,?)");
            pst.setString(1, mod.getNome());
            pst.setInt(2, mod.getCod_estado());
            pst.execute();
            JOptionPane.showMessageDialog(null,"Dados Inseridos com Sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro na Inserção de Dados. \n ERRO: "+ex);
        }
        
        
        
    }
    
}
