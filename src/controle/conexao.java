package controle;

import java.sql.*;
import javax.swing.*;

public class conexao{
    
   //final private String driver = "sun.jdbc.odbc.JdbcOdbcDriver";
   
   final private String driver = "com.mysql.jdbc.Driver";
   

    //final private String url = "jdbc:odbc:estoque";
    final private String url = "jdbc:mysql://127.0.0.1/estoque";
    final private String usuario = "root";
    final private String senha = "root";
    private Connection conexao;
    public Statement statement;
    public ResultSet resultset;
    
    
    public boolean conecta(){
        
        boolean result = true;
        try{
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, usuario, senha);
            JOptionPane.showMessageDialog(null,"Conectou");
            
        }catch(ClassNotFoundException Driver){
            JOptionPane.showMessageDialog(null,"Driver Não Localizado: " +Driver);
            result = false;
            
        } catch(SQLException Fonte){
             JOptionPane.showMessageDialog(null,"Deu erro na conexão "+ "com a fonte de dados: " +Fonte);
            result = false;
            
            
        }
        return result;
    }
    
   /* public static void Conectar() {
       System.out.println("Conectando ao banco...");
       try {
       Class.forName("driver");
      conexao =  DriverManager.getConnection("jdbc:mysql://127.0.0.1/estoque","root","root");
       System.out.println("Conectado.");
      } catch (ClassNotFoundException ex) {
    System.out.println("Classe não encontrada, adicione o driver nas bibliotecas.");
    //Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
    } catch(SQLException e) {
    System.out.println(e);
    throw new RuntimeException(e);
    }
 
   }*/
    
    public void desconecta(){
        boolean result = true;
        try{
            conexao.close();
            JOptionPane.showMessageDialog(null,"Banco Fechado");
        } catch(SQLException fecha){
            JOptionPane.showMessageDialog(null,"Não foi possivel " + "fechar o banco de dados: " +fecha);
            result  = false;
            
        }
        
    }
    
    public void executeSQL(String sql){
        try{
            statement = conexao.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY); // esses parametros são mecessarios para navegar para ambos os lados - Frente e Trás
            resultset = statement.executeQuery(sql);
            
        }catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "Não foi possivel " + " executar o comando sql,"+ sqlex+", O sql passado foi" + sql);
        }
        
        
    }
    
    
    
}

