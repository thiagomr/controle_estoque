/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author thiag
 */
public class ConectaBanco {
    
   public Statement stm; // tem que ser do tipo java.sql // Responsavel por preparar e realizar pesquisas no banco de dados
   public ResultSet rs;  // tem que ser do tipo java.sql  // reponsavel por armazenar o resultado de uma pesquisa passada para o statement
   private String driver = "org.postgresql.Driver"; //Responsável por identificar o serviço de banco de dados
   private String caminho = "jdbc:postgresql://localhost:5432/controle_estoque"; // Responsável por
   private String usuario = "postgres";
   private String senha = "root";
   public Connection conn; // tem que ser do tipo java.sql // responsavel por realizar a conexao com o banco de dados
   
   
   public void conexao(){ // metodo responsavel por realiar a conexão com o banco 
       try {
           System.setProperty("jdbc.Drivers", driver); // seta a propriedade do driver de conexão
           conn = DriverManager.getConnection(caminho, usuario, senha); // realiza a conexao com  o banco de dado // a variavel conn recebe um boleano como resposta do DriverManager
         //  JOptionPane.showMessageDialog(null, "Conectou!");
       } catch (SQLException ex) {
          JOptionPane.showMessageDialog(null, "Erro de Conexão! \n Erro: " +ex.getMessage());
       }
       
   }
   
   public void executaSQL(String sql){
       try {
           stm = conn.createStatement(rs.TYPE_SCROLL_SENSITIVE, rs.CONCUR_READ_ONLY);
           rs = stm.executeQuery(sql);
           
       } catch (SQLException ex) {
           //JOptionPane.showMessageDialog(null, "Erro de ExecutaSQL! \n Erro: " +ex.getMessage());
       }
   }
   
   public void desconecta(){
       try {
           conn.close(); // fecha a conexao com o bds
          // JOptionPane.showMessageDialog(null, "Desconectado com sucesso!");
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao fechar a conexão!");
       }
       
   }
    
    
}
